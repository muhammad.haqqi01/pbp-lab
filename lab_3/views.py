from django.forms.widgets import Input
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm

# Create your views here.

@login_required(login_url='/admin/login/')
def index(request):
    friend = Friend.objects.all()
    response = {'friends' : friend}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)
    if form.is_valid():
        form.save()
        return index(request)
    return render(request, 'lab3_form.html', {'form': form})
