import 'package:flutter/material.dart';

import 'widget/card_pendaftar.dart';
import 'dummy_data.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Project Channel',
      theme: ThemeData(
        scaffoldBackgroundColor: const Color(0xF8F8F8F8),
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Detail Project'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {

    bool isScreenWide = MediaQuery.of(context).size.width >= 480;

    return Scaffold(
      appBar: AppBar(
        title: Text(
            widget.title,
        ),
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            Container(
              height: 120,
              width: double.infinity,
              padding: EdgeInsets.all(20),
              alignment: Alignment.centerLeft,
              color: Theme.of(context).accentColor,
              child: Text(
                'Muhammad Haqqi Al Farizi',
                style: TextStyle(
                    fontWeight: FontWeight.w900,
                    fontSize: 20,
                    color: const Color(0xFFFFFFFF)),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            ListTile(
              leading: Icon(
                Icons.edit,
                size: 20,
              ),
              title: Text(
                "Edit Profil",
                style: TextStyle(
                  fontFamily: 'RobotoCondensed',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.logout,
                size: 20,
              ),
              title: Text(
                "Logout",
                style: TextStyle(
                  fontFamily: 'RobotoCondensed',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child:Center(
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.80,
                margin: EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 20.0),
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 40.0),
                child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 20.0),
                        child: Text(
                          "TP2 yang Sangat Pain",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              letterSpacing: 1.2,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                        child: Text(
                            "TP2 adalah singkatan dari Tugas Pain 2. Tugas ini adalah tugas yang telah menjatuhkan "
                                "mental saya sebagai mahasiswa pacil. Saya menghabiskan full 3 hari untuk mendapatkan nilai B. TPain2.",
                          style: TextStyle(
                            height: 1.6,
                          ),
                        ),
                      ),
                      Flex(
                        direction: isScreenWide ? Axis.horizontal : Axis.vertical,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: isScreenWide ? EdgeInsets.fromLTRB(0, 0, 7.5, 0) : EdgeInsets.fromLTRB(0, 5, 0, 0),
                            child: ElevatedButton(
                              onPressed: () {},
                              child: Text("Edit Proyek"),
                              style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                                primary: Colors.blue[400],
                              ),
                            ),
                          ),
                          Container(
                            margin: isScreenWide ? EdgeInsets.fromLTRB(7.5, 0, 0, 0) : EdgeInsets.fromLTRB(0, 20, 0.0, 0),
                            child: ElevatedButton(
                              onPressed: () {},
                              child: Text("Tutup Proyek"),
                              style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
                                primary: Colors.red[400],
                              ),
                            ),
                          )
                        ],
                      )
                    ]),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.80,
                margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
                padding: EdgeInsets.fromLTRB(10.0, 40.0, 10.0, 20.0),
                alignment: Alignment.centerLeft,
                child: Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                        child: Text(
                          "Struktur Data & Algoritma",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              letterSpacing: 0.5,
                              fontSize: 14,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Pemilik proyek",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              "Muhammad Haqqi Al Farizi",
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Bayaran",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              "Rp10.000.000",
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Skill yang dibutuhkan",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              "BS Tree, Sorting (sort of), LinkedIn",
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Jumlah orang yang dibutuhkan",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              "1000 Orang",
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Estimasi waktu",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              "1 Tahun",
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
                            child: Text(
                              "Kontak",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20.0, 0.0, 10.0, 20.0),
                            child: Text(
                              "sdapain@ui.ac.id",
                              textAlign: TextAlign.left,
                            ),
                          )],
                      ),
                    ]),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                width: MediaQuery.of(context).size.width * 0.80,
                margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                      child: Text(
                        "Daftar Pelamar",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            letterSpacing: 1.2,
                            fontSize: 18,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Flex(
                        direction: Axis.vertical,
                        children: DUMMY_PENDAFTAR.map((data) => CardPendaftar(data.name, data.description)).toList(),
                    ),
                  ],
                ),
              )
            ]
          )
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.white,
        selectedItemColor: Colors.indigo,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.question_answer),
            label: 'FAQ',
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.library_add),
            label: 'Buat proyek',
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.library_books),
            label: 'Proyek saya',
          ),
        ],
      ),
    );
  }
}
