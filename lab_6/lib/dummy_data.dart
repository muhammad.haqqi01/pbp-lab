import 'package:flutter/material.dart';

import './models/pendaftar.dart';

const DUMMY_PENDAFTAR = const [
  Pendaftar(
      name: 'Muhammad Haqqi Al Farizi',
      description: 'Orang stressss. Hobinya kena mental tiap hari, tiap jam'
  ),
  Pendaftar(
      name: 'Richard bin Abdul Mukhlis',
      description: 'Orang aneh, pintar, gak kenal juga aku ini siapa aslinya'
  ),
  Pendaftar(
      name: 'Mukhlis bin Ting',
      description: 'ACC aku di project ini, saya sepuh SDA terutama AVL Tree pain'
  ),
  Pendaftar(
      name: 'Ganteng',
      description: 'Saya ganteng, pintar, berbudi pekerti, cerdas, gila, stres, wibu, punya mental issue'
  ),
];