import 'package:flutter/material.dart';

import '../main.dart';

class CardPendaftar extends StatelessWidget {
  final String name;
  final String description;

  CardPendaftar(this.name, this.description);

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: new Color(0xE9E9E9E9),
            width: 1.0,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(40, 30, 40, 30),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
                child: Text(
                    name,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                ),
              ),
              Container(
                child: Text(description),
              )
            ],
          ),
        )
      );
  }
}
