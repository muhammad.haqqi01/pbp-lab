import 'package:flutter/material.dart';

class Pendaftar {
  final String name;
  final String description;

  const Pendaftar({
    required this.name,
    required this.description,
  });
}
