from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm

# Gak bisa make { % load staticfiles % }
# https://stackoverflow.com/questions/55929472/django-templatesyntaxerror-staticfiles-is-not-a-registered-tag-library

# Create your views here.
def index(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab4_index.html', response)

def note_list(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    if form.is_valid():
        form.save()
        return index(request)
    return render(request, 'lab4_form.html', {'form': form})