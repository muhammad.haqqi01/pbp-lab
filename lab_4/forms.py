from django import forms
from django.db import models
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"
        labels = {
            'to': 'Tujuan',
            'fromm': 'Pengirim',
            'title': 'Judul',
            'message': 'Pesan'
        }
        widgets = {
            'to': forms.TextInput(attrs={'class': 'short field'}),
            'fromm': forms.TextInput(attrs={'class': 'short field'}),
            'title': forms.TextInput(attrs={'class': 'short field'}),
            'message': forms.Textarea(attrs={'class': 'message field'}),
        }
