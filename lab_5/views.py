from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponse
from django.core import serializers
#from .forms import NoteForm

# Gak bisa make { % load staticfiles % }
# https://stackoverflow.com/questions/55929472/django-templatesyntaxerror-staticfiles-is-not-a-registered-tag-library

# Create your views here.
def index(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab5_index.html', response)
 
def get_note(request, id):
    # dictionary for initial data with
    # field names as keys
    note = Note.objects.filter(pk = id)
    data = serializers.serialize('json', note)
    return HttpResponse(data, content_type="application/json")
