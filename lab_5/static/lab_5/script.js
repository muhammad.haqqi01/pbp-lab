$(window).on('load', function () {
    loadTables();
});

$("#close").click(function(){
    $("#modal").css("display", "none");
    $("#modalDetails").html('')
})

$(document).ready(function(){
    $("body").on('click', '.v', function(){
        $("#modal").css({"display":"flex", "flex-direction": "column", "justify-content": "space-between"});
        $.ajax({
            url: "../lab-5/notes/" + this.id,
            type: 'GET',
            dataType: 'json',
            success: function(response){
                console.log(response)
                $("#modalDetails").append("<p id='to'>To : " + response[0].fields.to + "</p><p id='from'>From  : " +  + response[0].fields.fromm +  "</p><p id='title'>Title : "  + response[0].fields.title + "</p><p id='message'>Message : <br> " + response[0].fields.message + "</p>")
            },
            error: function(response){
                alert("cok")
            }
        });
    });    
});

function loadTables() {
    $.ajax({
        url: "../lab-2/json",
        type: 'GET',
        dataType: 'json',
        success: function(response){
            $("#table").append('<tr><th>To</th><th>From</th><th>Title</th><th>Message</th><th id="HAction">Actions</th></tr>')
            //loop through json array
            $(response).each(function(index, value) {
                $("#table").append("<tr><td>" + value.fields.to + "</td><td>" + value.fields.fromm + "</td><td>" + value.fields.title + "</td><td>" + value.fields.message + "</td><td><a class='action v' id=" + value.pk + ">V</a><a class='action e'>E</a><a class='action d'>D</a></td></tr>")
            })
        }
    });
}
