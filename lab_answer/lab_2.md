1. Apakah perbedaan antara JSON dan XML? <br>
Dari namanya saja, bisa dilihat perbedaannya kalau JSON adalah Object JavaScript, sedangkan XML merupakan Markup Language.
Dari isi filenya, bisa dilihat juga kalau JSON hanyalah berisi Object dari data-data yang ada pada web. Kalau XML, isinya sudah terstruktur
seperti HTML. Karena isi dari JSON cuma object data dan XML berisi data udh terstruktur, JSON memiliki ukuran yang lebih kecil
daripada XML sehingga dalam pengiriman file, data dengan JSON lebih cepat terkirim, dibaca, dan ditulis. 
JSON juga bisa menggunakan array sementara XML tidak.

2. Apakah perbedaan antara HTML dan XML? <br>
Inti perbedaan dari keduanya adalah HTML digunakan untuk menampilkan data pada web sementara XML digunakan untuk mengirim data pada web, jadi XML lebih fokus pada isi data sedangkan HTML fokus pada cara datanya ditampilkan.
HTML adalah bahasa yang static, sehingga isi(HTML)nya hanya bisa diupdate secara manual. Karena data bisa berubah-ubah, XML adalah bahasa yang dinamis yang bisa berubah tergantung pada datanya.
HTML adalah bahasa yang case insensitive, jadi jika misal kita mengetik "< BR >" atau "< br >", keduanya dianggap sama. XML adalah bahasa yang case insensitive.
