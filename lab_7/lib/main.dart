import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MaterialApp(
    title: "Edit Project",
    theme: ThemeData(
      scaffoldBackgroundColor: const Color(0xF8F8F8F8),
      primarySwatch: Colors.blue,
    ),
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  var edited = 0.0 ;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Project"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.all(20.0),
                  child: Text(
                    "Edit Project",
                    style: TextStyle(
                        fontSize: 24.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    initialValue: "Muhammad Haqqi Al Farizi",
                    decoration: new InputDecoration(
                      hintText: "contoh: Muhammad Haqqi Al Farizi",
                      labelText: "Nama Pemilik",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if(value != null){
                        if (value.isEmpty) {
                          return 'Nama tidak boleh kosong';
                        }
                        return null;
                      }
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    initialValue: "TP2 yang Sangat Pain",
                    decoration: new InputDecoration(
                      hintText: "contoh: Kacamata Transparan",
                      labelText: "Nama Proyek",
                      icon: Icon(Icons.build),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if(value != null){
                        if (value.isEmpty) {
                          return 'Nama proyek tidak boleh kosong';
                        }
                        return null;
                      }
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    initialValue: "Struktur Data & Algoritma",
                    decoration: new InputDecoration(
                      hintText: "contoh: Web Development",
                      labelText: "Kategori",
                      icon: Icon(Icons.category),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if(value != null){
                        if (value.isEmpty) {
                          return 'Kategori tidak boleh kosong';
                        }
                        return null;
                      }
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    initialValue: "10000000",
                    decoration: new InputDecoration(
                      hintText: 'contoh: 100000',
                      labelText: "Bayaran",
                      icon: Icon(Icons.money),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    validator: (value) {
                      if(value != null){
                        if (value.isEmpty) {
                          return 'Bayaran tidak boleh kosong';
                        }
                        return null;
                      }
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 120.0,
                    child: TextFormField(
                      initialValue: "TP2 adalah singkatan dari Tugas Pain 2. Tugas ini adalah tugas yang telah menjatuhkan "
                          "mental saya sebagai mahasiswa pacil. Saya menghabiskan full 3 hari untuk mendapatkan nilai B. TPain2.",
                      maxLines: 5,
                      decoration: new InputDecoration(
                        hintText: "contoh: Proyek ini adalah proyek yang ditujukan untuk membuat mahasiswa indonesia menjadi sehat mental",
                        labelText: "Deskripsi",
                        icon: Icon(Icons.description),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if(value != null){
                          if (value.isEmpty) {
                            return 'Deskripsi tidak boleh kosong';
                          }
                          return null;
                        }
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    initialValue: "1 Tahun",
                    decoration: new InputDecoration(
                      hintText: "contoh: 9 Bulan",
                      labelText: "Estimasi Waktu",
                      icon: Icon(Icons.timer),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if(value != null){
                        if (value.isEmpty) {
                          return 'Estimasi waktu tidak boleh kosong';
                        }
                        return null;
                      }
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 120.0,
                    child: TextFormField(
                      initialValue: "BS Tree, Sorting (sort of), LinkedIn",
                      maxLines: 5,
                      decoration: new InputDecoration(
                        hintText: "contoh: \n React, Flutter, Panjat Tebing, Struktur Data",
                        labelText: "Skill yang dibutuhkan",
                        icon: Icon(Icons.handyman),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if(value != null){
                          if (value.isEmpty) {
                            return 'Skill yang dibutuhkan tidak boleh kosong';
                          }
                          return null;
                        }
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 120.0,
                    child: TextFormField(
                      initialValue: "sdapain@ui.ac.id",
                      maxLines: 5,
                      decoration: new InputDecoration(
                        hintText: "contoh: \n Gmail: abc@gmail.com \n Line: @sule_elus",
                        labelText: "Kontak",
                        icon: Icon(Icons.contact_mail),
                        border: OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if(value != null){
                          if (value.isEmpty) {
                            return 'Kontak pemilik tidak boleh kosong';
                          }
                          return null;
                        }
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    initialValue: "1000",
                    decoration: new InputDecoration(
                      hintText: 'contoh: 15',
                      labelText: "Jumlah Orang",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    validator: (value) {
                      if(value != null){
                        if (value.isEmpty) {
                          return 'Jumlah orang tidak boleh kosong';
                        }
                        return null;
                      }
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8.0),
                  child: RaisedButton(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.blue,
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Mengedit Data')),
                        );
                        setState(() {
                          edited = 1.0;
                        });
                      }
                    },
                  ),
                ),
                Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Opacity(
                      opacity: edited,
                      child: Text(
                        "Proyek berhasil diedit!",
                      ),
                    ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}